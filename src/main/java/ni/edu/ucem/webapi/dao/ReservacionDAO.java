package ni.edu.ucem.webapi.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.modelo.Reservacion;;


public interface ReservacionDAO 
{
	public Reservacion obtenerPorId(final int pId);

    public int contar();
    
    public List<Reservacion> obtenerTodos(final int offset, final int limit);

    public void agregar(final Reservacion pReservacion);

    public void guardar(final Reservacion pReservacion);

    public void eliminar(final int pId);

	public Cupo obtenerDisponiblidadCupo(Date fechaIngreso, Date fechaSalida, Optional<Integer> categoriaId, Optional<Integer> offset, Optional<Integer> limit);

}


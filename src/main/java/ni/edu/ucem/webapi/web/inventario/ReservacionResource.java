package ni.edu.ucem.webapi.web.inventario;

import java.util.ArrayList;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ni.edu.ucem.webapi.core.ApiResponseReservaciones;
import ni.edu.ucem.webapi.core.ApiResponseReservaciones.Status;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.modelo.ReservacionCuartoHuesped;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.serviceImpl.ReservacionServiceImpl;
import ni.edu.ucem.webapi.serviceImpl.InventarioServiceImpl;

@RestController
@RequestMapping("/v1/reservaciones")
public class ReservacionResource 
{
	  private final ReservacionServiceImpl reservacionService;
	  private final InventarioServiceImpl inventarioService;

	    public ReservacionResource(ReservacionServiceImpl reservacionService, InventarioServiceImpl inventarioService) {
	        this.reservacionService = reservacionService;
	        this.inventarioService = inventarioService;
	    }
	     

    @RequestMapping(method = RequestMethod.GET, produces="application/json")
    public java.util.List<ReservacionCuartoHuesped> obtenerReservaciones(
            @RequestParam(value = "offset", required = false, defaultValue ="0") final Integer offset,
            @RequestParam(value = "limit", required = false, defaultValue="0") final Integer limit)
    {
    	
        final Filtro paginacion = new Filtro.Builder()
                .paginacion(offset, limit)
                .build();

        Pagina<Reservacion> pagina;
        pagina = this.reservacionService.obtenerTodasReservaciones(paginacion);
        
       java.util.List<ReservacionCuartoHuesped> datos = new ArrayList<ReservacionCuartoHuesped>();
       java.util.List<Reservacion> lista2 = pagina.getData();
       
       for (Reservacion elemento: lista2)
       {
           final Cuarto cuarto = this.inventarioService.obtenerCuarto(elemento.getCuarto());
           final Huesped huesped = this.inventarioService.obtenerHuesped(elemento.getHuesped());
           
//               ReservacionCuartoHuesped hh = new ReservacionCuartoHuesped();
  		 ReservacionCuartoHuesped hh = new ReservacionCuartoHuesped(elemento.getId(),elemento.getDesde(),
  				elemento.getHasta(),cuarto,huesped); 
  		 
  		datos.add(hh);
       }
       return datos;
        //return new ListApiResponseReservaciones<Reservacion>(Status.OK, pagina);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces="application/json")
    public ReservacionCuartoHuesped obtener(@PathVariable("id") final int id)
    {

         final Reservacion reservacion = this.reservacionService.obtenerReservacion(id);
         final Cuarto cuarto = this.inventarioService.obtenerCuarto(reservacion.getCuarto());
         final Huesped huesped = this.inventarioService.obtenerHuesped(reservacion.getHuesped());
         
//             ReservacionCuartoHuesped hh = new ReservacionCuartoHuesped();
		 ReservacionCuartoHuesped hh = new ReservacionCuartoHuesped(reservacion.getId(),reservacion.getDesde(),
		 reservacion.getHasta(),cuarto,huesped);
     
         return hh;
    } 
    
    
    @RequestMapping(method = RequestMethod.POST,
            produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponseReservaciones guardarReservacion(@Valid @RequestBody final Reservacion pReservacion, BindingResult result) 
    {
        if(result.hasErrors())
        {
            throw new IllegalArgumentException(result.getFieldError().getDefaultMessage());
        }
        
        this.reservacionService.agregarReservacion(pReservacion);
        return new ApiResponseReservaciones(Status.OK, pReservacion);
    }
    
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponseReservaciones guardarReservacionConFormData(final Integer id, Date desde, Date hasta,
            final Integer cuarto, final Integer huesped) 
    {
    	Reservacion  pReservacion= new Reservacion(id, desde, hasta, cuarto, huesped);
        this.reservacionService.agregarReservacion(pReservacion);
        return new ApiResponseReservaciones(Status.OK, pReservacion);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT,
            produces="application/json")
    public ApiResponseReservaciones guardarReservacion(@PathVariable("id") final int id, 
            @RequestBody final Reservacion reservacionActualizada) 
    {
        final Reservacion pReservacion = new Reservacion(id,
        		reservacionActualizada.getDesde(),
        		reservacionActualizada.getHasta(),
        		reservacionActualizada.getCuarto(),
        		reservacionActualizada.getHuesped());
        this.reservacionService.guardarReservacion(pReservacion);
        return new ApiResponseReservaciones(Status.OK, pReservacion);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE,
            produces="application/json")
    public ApiResponseReservaciones eliminarReservacion(@PathVariable("id") final int pId) 
    {
        final Reservacion reservacion = this.reservacionService.obtenerReservacion(pId);
        this.reservacionService.eliminarReservacion(reservacion.getId());
        return new ApiResponseReservaciones(Status.OK,null);
    }
}


package ni.edu.ucem.webapi.daoImpl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import ni.edu.ucem.webapi.dao.ReservacionDAO;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.modelo.Reservacion;;

@Repository
public class ReservacionDAOImpl implements  ReservacionDAO
{
    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    
    @Autowired
    public ReservacionDAOImpl(final JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
        this.namedJdbcTemplate = new NamedParameterJdbcTemplate(
                this.jdbcTemplate.getDataSource());
    }

    @Override
    public Reservacion obtenerPorId(final int pId) 
    {
    	String sql = "select * from reservacion where id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{pId}, 
                new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
    }

    public int contar()
    {
        final String sql = "select count(*) from reservacion";
        return this.jdbcTemplate.queryForObject(sql, Integer.class);
    }
        
    @Override
    public List<Reservacion> obtenerTodos(final int pOffset, final int pLimit)
    {
        final String sql = "select * from reservacion offset ? limit ?";
        return this.jdbcTemplate.query(sql, new Object[]{pOffset, pLimit},
                new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
    }
   
    @Override
    public void agregar(final Reservacion pReservacion) 
    {
        final String sql = new StringBuilder()
                .append("INSERT INTO reservacion")
                .append(" ")
                .append("(desde, hasta, cuarto, huesped)")
                .append(" ")
                .append("VALUES(?,?,?,?)")
                .toString();
        final Object[] parametros = new Object[4];
        parametros[0] = pReservacion.getDesde();
        parametros[1] = pReservacion.getHasta();
        parametros[2] = pReservacion.getCuarto();
        parametros[3] = pReservacion.getHuesped();
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void guardar(final Reservacion pReservacion) 
    {
        final String sql = new StringBuilder()
                .append("UPDATE reservacion")
                .append(" ")
                .append("SET desde = ?, hasta = ?")
                .append(",cuarto = ?, huesped = ?")
                .append(" ")
                .append("WHERE id = ?")
                .toString();
        final Object[] parametros = new Object[5];
        parametros[0] = pReservacion.getDesde();
        parametros[1] = pReservacion.getHasta();
        parametros[2] = pReservacion.getCuarto();
        parametros[3] = pReservacion.getHuesped();
        parametros[4] = pReservacion.getId();
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void eliminar(final int pId) 
    {
        final String sql = "delete from reservacion where id = ?";
        this.jdbcTemplate.update(sql, new Object[]{pId});
    }

    
    @Override
    public Cupo obtenerDisponiblidadCupo(final Date fechaIngreso, final Date fechaSalida, final Optional<Integer> categoriaId,final Optional<Integer> offset,
                                         final Optional<Integer> limit) 
    {

        Map<String, Object> parametros = new HashMap<>();
        parametros.put("fechaIngreso", fechaIngreso);
        parametros.put("fechaSalida", fechaSalida);

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM cuarto ");
        sql.append("WHERE id NOT IN ( ");
        sql.append(" SELECT cuarto FROM reservacion");
        sql.append(" WHERE (desde <= :fechaIngreso OR hasta <= :fechaIngreso)");
        sql.append(" OR (desde <= :fechaSalida OR hasta <= :fechaSalida)");
        sql.append(")");

        categoriaId.ifPresent(value ->{
            sql.append(" ");
            sql.append("AND categoria = :categoriaId");
            parametros.put("categoriaId", categoriaId);
        });

        java.util.List<Cuarto> cuartosDisponibles = this.namedJdbcTemplate.query(sql.toString(), parametros,
                                            new BeanPropertyRowMapper<Cuarto>(Cuarto.class));

        return new Cupo(fechaIngreso, fechaSalida, cuartosDisponibles);
    }
}

package ni.edu.ucem.webapi.service;

import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.modelo.Pagina;

import java.util.Date;
import java.util.Optional;

import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.modelo.Filtro;

public interface ReservacionService {

	public void agregarReservacion(final Reservacion pReservacion);

    public void guardarReservacion(final Reservacion pReservacion);

    public void eliminarReservacion(int id);
    
    public Reservacion obtenerReservacion(final int id);

    public Pagina<Reservacion> obtenerTodasReservaciones(final Filtro paginacion);
 
	public Cupo obtenerDisponiblidadCupo(Date fechaIngreso, Date fechaSalida, Optional<Integer> categoriaId, Optional<Integer> offset, Optional<Integer> limit);
    
}

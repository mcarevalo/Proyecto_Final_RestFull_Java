package ni.edu.ucem.webapi.dao;

import java.util.List;
import java.util.Optional;

import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Huesped;

public interface CuartoDAO 
{
	public Cuarto obtenerPorId(final int pId, final String... proyeccion);

    public int contar();
    
    public int contarPorCategoria(final int pCategoriaId);
    
    public List<Cuarto> obtenerTodos(final int offset, final int limit, Optional<String> sort, Optional<String> sortOrder);

    public List<Cuarto> obtenerTodosPorCategoriaId(final int pCategoriaId, final int offset, final int limit);

    public void agregar(final Cuarto pCuarto);

    public void guardar(final Cuarto pCuarto);

    public void eliminar(final int pId);
    
    public Huesped obtenerPorIdHuesped(final int pId);
}

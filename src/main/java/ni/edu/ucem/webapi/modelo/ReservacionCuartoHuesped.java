package ni.edu.ucem.webapi.modelo;

import java.util.Date;


public class ReservacionCuartoHuesped {
    
	private Integer id;
	   
    private Date desde;

    private Date hasta;

    private Cuarto cuarto;
  
    private Huesped huesped;
	
    public ReservacionCuartoHuesped() {
    	
    }

    public ReservacionCuartoHuesped(Integer id, Date desde, Date hasta, Cuarto cuarto, Huesped huesped) {
        this.id = id;
        this.desde = desde;
        this.hasta = hasta;
        this.cuarto = cuarto;
        this.huesped = huesped;
    }

   
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDesde() {
        return desde;
    }

    public void setDesde(Date desde) {
        this.desde = desde;
    }

    public Date getHasta() {
        return hasta;
    }

    public void setHasta(Date hasta) {
        this.hasta = hasta;
    }

    public Cuarto getCuarto() {
        return cuarto;
    }

    public void setCuarto(Cuarto cuarto) {
        this.cuarto = cuarto;
    }

    public Huesped getHuesped() {
        return huesped;
    }

    public void setHuesped(Huesped huesped) {
        this.huesped = huesped;
    }
  
}

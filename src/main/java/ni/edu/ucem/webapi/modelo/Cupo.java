package ni.edu.ucem.webapi.modelo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

import javax.validation.constraints.NotNull;

public class Cupo {
    
    @NotNull(message = "La fecha de Ingreso desde es requerida.")
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date fechaIngreso;
      
    @NotNull(message = "La fecha de Salida es requerida.")
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date fechaSalida;


    private  java.util.List<Cuarto> cuartos;

    public Cupo()
    {
    
    }
     
    public Cupo(Date fechaIngreso, Date fechaSalida,  java.util.List<Cuarto> cuartos) {
        this.fechaIngreso = fechaIngreso;
        this.fechaSalida = fechaSalida;
        this.cuartos = cuartos;
    }
    
    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public  java.util.List<Cuarto> getCuartos() {
        return cuartos;
    }

    public void setCuartos( java.util.List<Cuarto> cuartos) {
        this.cuartos = cuartos;
    }
    
    
}
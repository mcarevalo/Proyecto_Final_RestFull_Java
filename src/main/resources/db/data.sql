INSERT INTO categoria_cuarto (nombre,descripcion,precio)
    VALUES ('Individual', 'Ideal para quienes viajan solos.',50.0);
INSERT INTO categoria_cuarto (nombre,descripcion,precio)
    VALUES ('Familiar', 'Ideal para quienes viajan en familia.',50.0);

INSERT INTO cuarto (numero, descripcion,categoria,modificado)
    VALUES(1,'Vista a la piscina',1,'2017-02-20 21:35:17.422');
INSERT INTO cuarto (numero, descripcion,categoria,modificado)
  VALUES(2,'Remodelado recientemente',1,'2017-02-20 21:35:17.422');
  
  
INSERT INTO huesped (id, nombre,email,telefono)
    VALUES(1,'Luisa Fuentes','luisa@gmail.com','76543213');
INSERT INTO huesped (id, nombre,email,telefono)
    VALUES(2,'Niall Castillo','niall@gmail.com','88576583');
    
INSERT INTO reservacion (id, desde,hasta,cuarto,huesped)
    VALUES(1,'2017-02-25 21:35:17.422','2017-02-26 21:35:17.422',1,1);
INSERT INTO reservacion (id, desde,hasta,cuarto,huesped)
    VALUES(2,'2017-02-25 21:35:17.422','2017-02-26 21:35:17.422',1,2);
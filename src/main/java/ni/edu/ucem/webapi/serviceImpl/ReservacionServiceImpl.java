package ni.edu.ucem.webapi.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ni.edu.ucem.webapi.dao.ReservacionDAO;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.modelo.Filtro;

import ni.edu.ucem.webapi.service.ReservacionService;

@Service
public class ReservacionServiceImpl implements ReservacionService 
{
    private final ReservacionDAO reservacionDAO;
    
    public ReservacionServiceImpl(final ReservacionDAO reservacionDAO)
    {
        this.reservacionDAO = reservacionDAO;
        
    }
    
    @Transactional
    @Override
    public void agregarReservacion(final Reservacion pReservacion) 
    {
        this.reservacionDAO.agregar(pReservacion);
    }

    @Transactional
    @Override
    public void guardarReservacion(final Reservacion pReservacion) 
    {
        if(pReservacion.getId() < 1)
        {
            throw new IllegalArgumentException("La Reservacion no existe");
        }
        this.reservacionDAO.guardar(pReservacion);
    }

    @Transactional
    @Override
    public void eliminarReservacion(final int pId) 
    {
        if(pId < 1)
        {
            throw new IllegalArgumentException("ID invalido. Debe ser mayor a cero");
        }
        this.reservacionDAO.eliminar(pId);
    }

    @Override
    public Reservacion obtenerReservacion(final int pId) 
    {
    	if (pId < 0) 
        {
            throw new IllegalArgumentException("ID inválido. debe ser mayor a cero.");
        }
        return this.reservacionDAO.obtenerPorId(pId);
    }
    
    @Override
    public Pagina<Reservacion> obtenerTodasReservaciones(final Filtro paginacion) 
    {
        List<Reservacion> reservacion;
        final int count = this.reservacionDAO.contar();
        if(count > 0)
        {
        	reservacion = this.reservacionDAO.obtenerTodos(paginacion.getOffset(),
                    paginacion.getLimit());
        }
        else
        {
        	reservacion = new ArrayList<Reservacion>();
        }
        return new Pagina<Reservacion>(reservacion, count,  paginacion.getOffset(), paginacion.getLimit());
    }

    @Transactional
    @Override
    public Cupo obtenerDisponiblidadCupo(final Date fechaIngreso, final Date fechaSalida, final Optional<Integer> categoriaId, final Optional<Integer> offset, final Optional<Integer> limit)
    {

        this.validarFechasReservacionCupos(fechaIngreso, fechaSalida);

        return this.reservacionDAO.obtenerDisponiblidadCupo( fechaIngreso,fechaSalida,categoriaId,offset,limit);
    }
    
    private void validarFechasReservacionCupos(final Date fechaIngreso,final Date fechaSalida)
    {
        DateTime fechaDesde = new DateTime(fechaIngreso);
        DateTime fechaHasta = new DateTime(fechaSalida);
        
        if(fechaDesde.isBefore(new DateTime(new Date()))){
            throw new IllegalArgumentException("La fecha de ingreso no puede ser menor a la fecha actual");
        }
        if(fechaDesde.isAfter(fechaHasta)){
            throw new IllegalArgumentException("La fecha de ingreso no puede ser mayor a la fecha salida.");
        }
    }
    
    
}

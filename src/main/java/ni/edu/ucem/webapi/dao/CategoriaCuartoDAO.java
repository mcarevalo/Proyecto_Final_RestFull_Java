package ni.edu.ucem.webapi.dao;

import java.util.List;
import java.util.Optional;

//import org.springframework.security.access.prepost.PreAuthorize;

import ni.edu.ucem.webapi.modelo.CategoriaCuarto;

public interface CategoriaCuartoDAO 
{
    
    public CategoriaCuarto obtenerPorId(final int pId, final String... proyeccion);

    public int contar();

    public List<CategoriaCuarto> obtenerTodos(final int offset, final int limit, Optional<String> sort, Optional<String> sortOrder);

    public void agregar(final CategoriaCuarto pCategoriaCuarto);

    public void guardar(final CategoriaCuarto pCategoriaCuarto);

    public void eliminar(final int pId);
}

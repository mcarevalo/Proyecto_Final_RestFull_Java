package ni.edu.ucem.webapi.service;

import java.util.Optional;

import ni.edu.ucem.webapi.modelo.CategoriaCuarto;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.modelo.Huesped;

public interface InventarioService 
{
    public void agregarCategoriaCuarto(final CategoriaCuarto pCategoriaCuarto);

    public void guardarCategoriaCuarto(final CategoriaCuarto pCategoriaCuarto);

    public void eliminarCategoriaCuarto(int id);
    
    public CategoriaCuarto obtenerCategoriaCuarto(final int id, final String... strings);

    public Pagina<CategoriaCuarto> obtenerTodosCategoriaCuartos(final Filtro paginacion, Optional<String> sort,Optional<String> sortOrder);

    public void agregarCuarto(final Cuarto pCuarto);

    public void guardarCuarto(final Cuarto pCuarto);

    public void eliminarCuarto(final int pCuarto);

    public Cuarto obtenerCuarto(final int pId, final String... strings);
    
    public Pagina<Cuarto> obtenerTodosCuarto(final Filtro paginacion, Optional<String> sort,Optional<String> sortOrder);

    public Pagina<Cuarto> obtenerTodosCuartoEnCategoria(final int pCategoriaCuarto, final Filtro paginacion);
    
    public Huesped obtenerHuesped(final int id);
    
    public void validarOrdenamiento(String sortOrder);

}

package ni.edu.ucem.webapi.daoImpl;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ni.edu.ucem.webapi.dao.CuartoDAO;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Huesped;

@Repository
public class CuartoDAOImpl implements CuartoDAO 
{
    private final JdbcTemplate jdbcTemplate;
   
    @Autowired
    public CuartoDAOImpl(final JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Huesped obtenerPorIdHuesped(final int pId) 
    {
    	String sql = "select * from huesped where id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{pId}, 
                new BeanPropertyRowMapper<Huesped>(Huesped.class));
    }
    
    @Override
    public Cuarto obtenerPorId(final int pId, final String... campos) 
    {
    	String fields = (campos != null && campos.length > 0 && campos[0] != null) ? campos[0] : "*";
        String sql = String.format("select %s from cuarto where id = ?", fields);
        return jdbcTemplate.queryForObject(sql, new Object[]{pId}, 
                new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
    }  
    
    @Override
    public int contar()
    {
        final String sql = "select count(*) from cuarto";
        return this.jdbcTemplate.queryForObject(sql, Integer.class);
    }
    
    @Override
    public int contarPorCategoria(final int categoriaId)
    {
        final String sql = "select count(*) from cuarto where ";
        return this.jdbcTemplate.queryForObject(sql, Integer.class);
    }

    /* Ordenamiento*/
    
    
    /* Paginado */
    
    @Override
    public List<Cuarto> obtenerTodos(final int pOffset, final int pLimit, final Optional<String> sort,
            final Optional<String> sortOrder) 
    {
    	 String sql = "select * from cuarto ";
    	 int oo = 0;
    	if(sort.isPresent())
    	{
    		List<String> jj= Arrays.asList(new String[]{sort.get()});
    		 
    		for (String elemento: jj)
    		{
    			if(elemento.trim().length() > 0)
    			{
        			
        			if(oo == 0)
        			{
        				sql = sql + " ORDER BY " ;	
        			}
        			sql = sql + " " + elemento;
        			oo ++; 
    			}
    		}
           
    	}
    	sql = sql + " ";
    	if(sort.isPresent() && oo > 0)
    	{
    		sql = sql + " " + sortOrder.orElse("ASC");
    	}
    	
    	sql = sql + " offset ? limit ? " ; 
        
    
        return this.jdbcTemplate.query(sql, new Object[]{pOffset, pLimit},
                new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
    }

    @Override
    public List<Cuarto> obtenerTodosPorCategoriaId(int pCategoriaId, int pOffset, int pLimit) 
    {
        final String sql = "select * from cuarto where categoria = ? offset ? limit ?";
        return this.jdbcTemplate.query(sql, new Object[]{pCategoriaId, pOffset, pLimit},
                new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
    }

    @Override
    public void agregar(final Cuarto pCuarto) 
    {
        final String sql = new StringBuilder()
                .append("INSERT INTO cuarto")
                .append(" ")
                .append("(numero, descripcion, categoria)")
                .append(" ")
                .append("VALUES (?, ?, ?)")
                .toString();
        final Object[] parametros = new Object[3];
        parametros[0] = pCuarto.getNumero();
        parametros[1] = pCuarto.getDescripcion();
        parametros[2] = pCuarto.getCategoria();
        this.jdbcTemplate.update(sql,parametros);
        
    }

    @Override
    public void guardar(final Cuarto pCuarto) 
    {        
       final String sql = new StringBuilder()
                .append("UPDATE cuarto")
                .append(" ")
                .append("set numero = ?")
                .append(",descripcion = ?")
                .append(",categoria = ?")
                .append(",modificado = ?")
                .append(" ")
                .append("where id = ?")
                .toString();
        final Object[] parametros = new Object[5];
        parametros[0] = pCuarto.getNumero();
        parametros[1] = pCuarto.getDescripcion();
        parametros[2] = pCuarto.getCategoria();
        parametros[3] = new Timestamp(new Date().getTime());
        parametros[4] = pCuarto.getId();
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void eliminar(final int pId) 
    {
        final String sql = "delete from cuarto where id = ?";
        this.jdbcTemplate.update(sql, new Object[]{pId});
    }
}

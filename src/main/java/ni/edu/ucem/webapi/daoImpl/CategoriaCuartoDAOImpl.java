package ni.edu.ucem.webapi.daoImpl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ni.edu.ucem.webapi.dao.CategoriaCuartoDAO;
import ni.edu.ucem.webapi.modelo.CategoriaCuarto;

@Repository
public class CategoriaCuartoDAOImpl implements CategoriaCuartoDAO 
{
    private final JdbcTemplate jdbcTemplate;
    
    @Autowired
    public CategoriaCuartoDAOImpl(final JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public CategoriaCuarto obtenerPorId(final int pId, final String... campos) 
    {
    	String filter = (campos != null && campos.length > 0 && campos[0] != null) ? campos[0] : "*";
    	String sql = String.format("select %s from categoria_cuarto where id = ?", filter);
    		return jdbcTemplate.queryForObject(sql, new Object[]{pId}, 
            new BeanPropertyRowMapper<CategoriaCuarto>(CategoriaCuarto.class));
    }

    public int contar()
    {
        final String sql = "select count(*) from categoria_cuarto";
        return this.jdbcTemplate.queryForObject(sql, Integer.class);
    }
        
    @Override
    public List<CategoriaCuarto> obtenerTodos(final int pOffset, final int pLimit, final Optional<String> sort,
            final Optional<String> sortOrder)
    {
        //final String sql = "select * from categoria_cuarto offset ? limit ?";
    	 String sql = "select * from categoria_cuarto ";
    	 int oo = 0;
    	if(sort.isPresent())
    	{
    		List<String> jj= Arrays.asList(new String[]{sort.get()});
    		 
    		for (String elemento: jj)
    		{
    			if(elemento.trim().length() > 0)
    			{
        			
        			if(oo == 0)
        			{
        				sql = sql + " ORDER BY " ;	
        			}
        			sql = sql + " " + elemento;
        			oo ++; 
    			}
    		}
           
    	}
    	sql = sql + " ";
    	if(sort.isPresent() && oo > 0)
    	{
    		sql = sql + " " + sortOrder.orElse("ASC");
    	}
    	
    	sql = sql + " offset ? limit ? " ;
    	
        return this.jdbcTemplate.query(sql, new Object[]{pOffset, pLimit},
                new BeanPropertyRowMapper<CategoriaCuarto>(CategoriaCuarto.class));    
    }
   
    @Override
    public void agregar(final CategoriaCuarto pCategoriaCuarto) 
    {
        final String sql = new StringBuilder()
                .append("INSERT INTO categoria_cuarto")
                .append(" ")
                .append("(nombre, descripcion, precio)")
                .append(" ")
                .append("VALUES(?,?,?)")
                .toString();
        final Object[] parametros = new Object[3];
        parametros[0] = pCategoriaCuarto.getNombre();
        parametros[1] = pCategoriaCuarto.getDescripcion();
        parametros[2] = pCategoriaCuarto.getPrecio();
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void guardar(final CategoriaCuarto pCategoriaCuarto) 
    {
        final String sql = new StringBuilder()
                .append("UPDATE categoria_cuarto")
                .append(" ")
                .append("SET nombre = ?, descripcion = ?")
                .append(",precio = ?")
                .append(" ")
                .append("WHERE id = ?")
                .toString();
        final Object[] parametros = new Object[4];
        parametros[0] = pCategoriaCuarto.getNombre();
        parametros[1] = pCategoriaCuarto.getDescripcion();
        parametros[2] = pCategoriaCuarto.getPrecio();
        parametros[3] = pCategoriaCuarto.getId();
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void eliminar(final int pId) 
    {
        final String sql = "delete from categoria_cuarto where id = ?";
        this.jdbcTemplate.update(sql, new Object[]{pId});
    }

}

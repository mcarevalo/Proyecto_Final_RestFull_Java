package ni.edu.ucem.webapi.web.inventario;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.service.ReservacionService;


@RestController
@RequestMapping("/v1/disponibilidad/cupos")
public class CupoResource 
{
	private final ReservacionService reservacionService;
    
    @Autowired
    public CupoResource(final ReservacionService reservacionService)
    {
    	this.reservacionService = reservacionService;
    }

    @RequestMapping(method = RequestMethod.GET, produces="application/json")
    public Cupo obtenerCupos(
    		@RequestParam(value = "fechaIngreso", required = true)@DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'") final Date fechaIngreso,
    		@RequestParam(value = "fechaSalida", required = true)@DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'") final Date fechaSalida,
            @RequestParam(value = "categoria", required = false) Optional<Integer> categoriaId,
            @RequestParam(value = "offset", required = false, defaultValue ="0")Optional<Integer> offset,
            @RequestParam(value = "limit", required = false, defaultValue="0") Optional<Integer> limit)
    {
        
    	return this.reservacionService.obtenerDisponiblidadCupo(fechaIngreso, fechaSalida, categoriaId, offset, limit);
    } 
    
}

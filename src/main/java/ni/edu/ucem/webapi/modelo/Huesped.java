package ni.edu.ucem.webapi.modelo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Huesped 
{
    private Integer id;
    
    @NotNull
    @NotEmpty(message = "El nombre es requerido.")
    @Pattern(regexp = "^[\\w ]+$")
    private String nombre;
    
    @NotNull
    @NotEmpty(message = "El email es requerido.")
    private String email;
    
    @NotNull
    @NotEmpty(message = "El telefono es requerido.")
    private String telefono;
    
    public Huesped()
    {
    }

    public Huesped(final String nombre, final String email, final String telefono) {
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
    }
    
    public Huesped(final Integer id, final String nombre, final String email, final String telefono) 
    {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getNombre() {
        return nombre;
    }
    public void setNombre(final String nombre) {
        this.nombre = nombre;
    }
    
    public String getEmail() {
        return email;
    }
    public void setEmail(final String email) {
        this.email = email;
    }
    
    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(final String telefono) {
        this.telefono = telefono;
    }
    
}
